import { markdownify } from "@lib/utils/textConverter";
import Cta from "./components/Cta";
import FeatureCard from "./components/FeatureCard";
function Apropos({ data }) {
  const { frontmatter } = data;
  const { title, infos, offers, team, cta, headquarter, email, history } = frontmatter;
  return (
<>
    <section className="section">
      <div className="container">
        {markdownify(title, "h1", "text-center font-normal")}
        <div className="section row  -mt-6">
          <div className="col-12 mt-6">
            <div className="p-12  shadow">
              <div className="relative">
                {markdownify(infos[0].title, "h3")}
              </div>
              {markdownify(infos[0].description, "p", "faq-body mt-4")}
            </div>
          </div>
        </div>
	{offers.map((offer, index) => (
	<div className="section utilities-body p-12 shadow -mt-6">
	    {markdownify(offer.title, "h3")}
            <div className="col-12 row mt-6">
            {offer.users.map((user, index) => (
		  <div className="md:col-4">
	            {markdownify(user.title, "h4")}
	            <ul>
		    {user.usages.map((use, index) => (
	 	      <li>
		        {markdownify(use, "p")}
		      </li>
		    ))}
	            </ul>
		</div>
	    ))}
            </div>
	  </div>
	  ))}
      </div>
    </section>
<section>
	<div className="container shadow p-12">
		{markdownify(history.title, "h4")}
		{markdownify(history.desc, "p")}
	</div>	
</section>
    <section className="section">
      <div className="container">
        <div className="section row  -mt-6">
          <div className="col-12 mt-6">
            <div className="p-12  shadow">
              <div className="relative">
		{markdownify(team.title, "h3")}
		{markdownify(team.desc, "p")}
  		<div className="grid mt-8 gap-x-8 gap-y-6 sm:grid-cols-2 lg:grid-cols-3">
        	{team.members.map((member, index) => (
          		<FeatureCard data={member} />
        	))}
              	</div>
        		{markdownify(team.members_asso_title, "h4")}
        	<div className="grid mt-8 gap-x-8 gap-y-6 sm:grid-cols-2 lg:grid-cols-3">
        		{team.members_asso.map((member, index) => (
        			<FeatureCard data={member} />
        		))}
              	</div>
            </div>
         </div>
       </div>
      </div>
     </div>
   </section>
<section>
	<div className="container shadow p-12">
		{markdownify(headquarter.title, "h4")}
		{markdownify(headquarter.Adresse, "p")}
		<a href={email.url}>{email.text}</a>
	</div>	
</section>
    <Cta cta={cta} />
	</>
  );
}

export default Apropos;
