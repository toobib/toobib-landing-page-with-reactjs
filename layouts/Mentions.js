import Link from "next/link";
import Cta from "./components/Cta";
import { markdownify } from "@lib/utils/textConverter";

function Mentions({ data }) {
  const {
    frontmatter: { title, post_scriptum, articles },
  } = data;
  return (
    <>
      <section className="section">
        <div className="container utilities-body">
          <h1 className="text-center font-normal">{title}</h1>
          <div className="section p-12 shadow mt-6">
            {articles.map((article, index) => (
		<div className="py-6">
		{markdownify(article.title, "h3")}
		{article.description && markdownify(article.description, "p")}
		{article.descriptions && markdownify(article.descriptions[0].text, "p")}
		{article.list && (
			<ul className="px-6 list-disc">
			{article.list.map((res, index) => (
				<li className="">
					{markdownify(res.text, "p")}	
				</li>
			))}
			</ul>
		)}
		{article.hds_websites && (
			<div>  
			{article.hds_websites.map((group, index) => (
			<div>  
			{markdownify(group.title, "h6")}	
			<ul className="px-6 list-disc">
				{group.list.map((item, index) => (
				<li className="">
					{markdownify(item.text, "p")}	
				</li>
			))}
			</ul>
			</div>
		))}
			</div>
		)}
		{article.compositions && (
			<ul className="px-6 list-disc">
			{article.compositions.map((compo, index) => (
				<li className="">
					{markdownify(compo.text, "p")}	
				</li>
			))}
			</ul>
		)}
		{article.descriptions && article.descriptions[1] && markdownify(article.descriptions[1].text, "p")}
		{article.data_no_hds && (
			<ul className="px-6 list-disc">
			{article.data_no_hds.map((data, index) => (
				<li className="">
					{markdownify(data.text, "p")}	
				</li>
			))}
			</ul>
		)}
		</div>
	    ))}
		{markdownify(post_scriptum, "p")}
        </div>
        </div>
      </section>
    </>
  );
}

export default Mentions;
