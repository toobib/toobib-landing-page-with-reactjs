import { markdownify } from "@lib/utils/textConverter";

const Validation = ({ data }) => {
  const { frontmatter: {title, description},
  } = data;

  return (
    <section className="section p-12">
      <div className="container p-12 shadow mt-6">
        {markdownify(title, "h1", "text-center font-normal")}
        {markdownify(description, "p", )}
      </div>
    </section>
  );
};

export default Validation;
