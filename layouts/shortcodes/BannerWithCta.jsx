import React from "react";
import Link from "next/link";
import Image from "next/image";

function BannerWithCta({ className = "", title = "", ctaLabel = "En savoir plus", ctaHref = "#", image = "", children }) {
  return (
    <section className={`section pb-[50px] ${className}`}>
      <div className="container">
        <div className="row text-center">
          <div className="mx-auto lg:col-10">
            <h1 className="font-primary font-bold">{title}</h1>

            {children}

            <Link
              className="btn btn-primary mt-4 not-prose"
              href={ctaHref}
            >
              {ctaLabel}
            </Link>
            {image && <Image
              className="mx-auto mt-12"
              src={image}
              width={750}
              height={390}
              alt="Banner"
              priority
            />}
          </div>
        </div>
      </div>
    </section>
  );
}

export default BannerWithCta;
