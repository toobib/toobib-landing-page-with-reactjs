import Link from "next/link";
import Cta from "./components/Cta";
import { markdownify } from "@lib/utils/textConverter";

function Don({ data }) {
	const {
frontmatter: { title, post_scriptum, articles },
	} = data;
	return (
			<>
			<section className="section">
			<div className="container utilities-body">
			<h1 className="text-center font-normal">{title}</h1>
			<div className="section p-12 shadow mt-6">
			{articles.map((article, index) => (
						<div className="py-6">
						{markdownify(article.title, "h3")}
						{article.description && markdownify(article.description, "p")}
						{article.descriptions[0].text !== 'helloasso' && article.descriptions && markdownify(article.descriptions[0].text, "p")}
						{article.descriptions[0].text == 'helloasso' && (
								<>			
									<iframe id="haWidget" className="w-full h-750 border-none px-3" allowtransparency="true" scrolling="auto" src="https://www.helloasso.com/associations/interhop/formulaires/1/widget" onload="window.scroll(0, this.offsetTop)"></iframe>
								<div id="haWidgetMobile">
								<p>Tu vas être transféré.e sur HelloAsso.com.</p>
								<p><a className="button alt text-center" type="blue" href="https://www.helloasso.com/associations/interhop/collectes/defendez-une-vision-ethique-et-respectueuses-des-droits-numeriques-en-sante">Faire un don</a></p>
								</div>	
								</>
								)}
						</div>
						))}
	</div>
		</div>
		</section>
		</>
		);
}

export default Don;
