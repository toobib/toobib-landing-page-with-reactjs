import { markdownify } from "@lib/utils/textConverter";

function Roadmap({ data }) {
  const { frontmatter } = data;
  const { title, mission, objs, par } = frontmatter;
  return (
    <section className="section">
      <div className="container">
        {markdownify(title, "h1", "text-center font-normal")}
        <div className="section row  -mt-6">
          {mission.map((description, index) => (
            <div key={index} className="col-12 mt-6 md:col-6">
              <div className="p-12 h-full shadow">
                <div className="faq-head relative">
                  {markdownify(description.title, "h4")}
                </div>
                {markdownify(description.answer, "p", "utilities-body mt-4")}
              </div>
            </div>
          ))}
      {objs.map((obj, index) => (
        <div key={index} className="col-12 mt-6 md:col-6">
          <div className="p-12 h-full shadow">
            <div className="faq-head relative">
              {markdownify(obj.title, "h4")}
            </div>
            <ul className="mt-5">
              {obj.answer.map((item, index) => (
          	  <li className="mb-[10px] leading-5" key={index}>
          	    {markdownify(item, "p", "utilities-body mt-4")}
          	  </li>
               ))}
            </ul>
          </div>
        </div>
      ))}
      </div>
      </div>
      <div className="container">
		<div className="secton p-12 shadow">
                <div className="relative">
                  {markdownify(par.title, "h3")}
                </div>
		<div>
		{par.goals.map((goal, index) => (
			<div className="my-8" key={index}>
              		{markdownify("En " + goal.year, "h4")}
			<ul className="mt-5">
			{goal.items.map((item, index) => (
				<li key={index}>
          	    			{markdownify(item, "p", "utilities-body mt-4")}
				</li>
			))}
			</ul>
			</div>
		))}
		</div>				
        	</div>
</div>
    </section>
  );
}

export default Roadmap;
