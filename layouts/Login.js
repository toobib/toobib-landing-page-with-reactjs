"use client"
import Link from "next/link";
import Cta from "./components/Cta";
import { markdownify } from "@lib/utils/textConverter";
import {signIn, signOut, useSession } from "next-auth/react";
import { getServerSession } from 'next-auth';
import { authOptions } from '@pages/api/auth/[...nextauth]';
import { useEffect, useState } from 'react';

function Login({ data }) {
		const {data: session} = useSession()
    const {
frontmatter: { title, post_scriptum, articles },
	} = data;
	if (session) {
		return (   
 			<>
        Signed in as {session.user.email} <br />
        <button onClick={() => signOut()}>Sign out</button>
      </>
		)
  }
  return (
			<>
			<section className="section">
			<div className="container utilities-body">
			<h1 className="text-center font-normal">{title}</h1>
			<div className="section p-12 shadow mt-6">
				Not signed in <br />
				<button onClick={() => signIn("keycloak")}>
					Signin with keycloak
				</button>
			</div>
		</div>
		</section>
		</>
		);
}

export default Login;
