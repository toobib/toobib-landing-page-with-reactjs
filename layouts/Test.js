import Link from "next/link";
import Cta from "./components/Cta";
import { markdownify } from "@lib/utils/textConverter";
import { Card } from '@codegouvfr/react-dsfr/Card';


function Test({ data }) {
  const {
    frontmatter: { title, post_scriptum, articles },
  } = data;
  return (
    <>
      <section className="section">
        <div className="container utilities-body">
          <h1 className="text-center font-normal">{title}</h1>
        </div>
        <Card linkProps={{href: "/my-page" }} />
      </section>
    </>
  );
}

export default Test;
