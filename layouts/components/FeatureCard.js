import { markdownify } from "@lib/utils/textConverter";
import Image from "next/image";
import React from "react";

function FeatureCard({ data }) {
  const { title, icon, desc, href = null } = data;
  const Tag = href ? "a" : "div";
  return (
    <Tag className="feature-card rounded-xl bg-white p-5 pb-8 text-center" href={href}>
      {icon && (
        <Image className="mx-auto" src={icon} width={30} height={30} alt="" />
      )}
      <div className="mt-4">
        {markdownify(title, "h3", "h5")}
        <p className="mt-3">{desc}</p>
      </div>
    </Tag>
  );
}

export default FeatureCard;
