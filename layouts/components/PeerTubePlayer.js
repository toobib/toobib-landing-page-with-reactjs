import ReactPlayer from 'react-player';

const PeerTubePlayer = ({ embedKey, title }) => {
	let srcVideo="https://peertube.interhop.org/videos/embed/" + embedKey
  return (
		<iframe title={title} style={{margin:'auto'}} width="560" height="315" src={srcVideo} frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>
)
 
};

export default PeerTubePlayer;
