import Link from "next/link";
import Cta from "./components/Cta";
import { markdownify } from "@lib/utils/textConverter";

function Statuts({ data }) {
  const {
    frontmatter: { title, intro, articles },
  } = data;
  return (
    <>
      <section className="section">
        <div className="container utilities-body">
          <h1 className="text-center font-normal">{title}</h1>
          <div className="section p-12 shadow text-center">
            {markdownify(intro.text, "p")}
          </div>
          <div className="section p-12 shadow mt-6">
            {articles.map((article, index) => (
		<div className="py-6">
		{markdownify(article.title, "h3")}
		{article.description && markdownify(article.description, "p")}
		{article.descriptions && markdownify(article.descriptions[0].text, "p")}
		{article.objects && (
			<ul className="px-6 list-disc">
			{article.objects.map((obj, index) => (
				<li className="">
					{markdownify(obj.text, "p")}	
				</li>
			))}
			</ul>
		)}
		{article.categories_members && (
		<div>
		<ul className="px-6 list-disc">
			{article.categories_members.map((cat, index) => (
			<li>
				{markdownify(cat.text, "p")}	
				{cat.explanations && (
					<ul className="px-6 list-disc">
					{cat.explanations.map((desc, index) => (
						<li className="">
							{markdownify(desc.text, "p")}
						</li>
					))}
					</ul>
				)}
				{cat.type_members && (
					<ul className="px-6 list-disc">
					{cat.type_members.map((type, index) => (
						<li className="py-1">
							{markdownify(type.title, "h6")}	
							{markdownify(type.desc, "p")}	
						</li>
					))}
					</ul>
				)}
			</li>
			))}
		</ul>
		</div>
		)}
		{article.resources && (
			<ul className="px-6 list-disc">
			{article.resources.map((res, index) => (
				<li className="">
					{markdownify(res.text, "p")}	
				</li>
			))}
			</ul>
		)}
		{article.reasons && (
			<ul className="px-6 list-disc">
			{article.reasons.map((reason, index) => (
				<li className="">
					{markdownify(reason.text, "p")}	
				</li>
			))}
			</ul>
		)}
		{article.rules && (
			<ul className="px-6 list-disc">
			{article.rules.map((rule, index) => (
				<li className="">
					{markdownify(rule.text, "p")}	
				</li>
			))}
			</ul>
		)}
		{article.compositions && (
			<ul className="px-6 list-disc">
			{article.compositions.map((compo, index) => (
				<li className="">
					{markdownify(compo.text, "p")}	
				</li>
			))}
			</ul>
		)}
		{article.descriptions && article.descriptions.length > 1 && markdownify(article.descriptions[1].text, "p")}
		</div>
	    ))}
        </div>
        </div>
      </section>
    </>
  );
}

export default Statuts;
