/**
 * @type {import('next').NextConfig}
 */

const nextConfig = {
	async redirects() {
		return [
			{
				source: '/maquette',
				destination: 'https://shorturl.at/zK234',
				basePath: false,
				permanent: false,
			},
		]
	},
};

module.exports = nextConfig;
