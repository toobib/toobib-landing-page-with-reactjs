# Page d'accueil Toobib réalisée avec Next.js

Toobib est une plateforme web permettant aux professionnel.le.s de santé de d'accéder à des services numériques libres.


## Conception

Nous utilisons penpot pour concevoir la [prochaine version de toobib](https://penpot.interhop.org/#/view/1f43cc2f-6ecb-8131-8002-6fca9a74db2a?section=interactions&index=0&share-id=7e202e91-92ad-8006-8003-19ade857756d).

Aujourd'hui, nous utilisons le modèle bigspring-light :
![bigspring-light](https://demo.gethugothemes.com/thumbnails/bigspring-light.png)

## Installation

Après avoir téléchargé le modèle, vous devez installer quelques prérequis. Vous pouvez ensuite l'exécuter sur votre hôte local. Vous pouvez consulter le fichier package.json pour voir quels scripts sont inclus.

### cloner le répertoire

```sh
git clone https://framagit.org/toobib/toobib-landing-page-with-reactjs.git
```

### Installer les prérequis (une fois par machine)

**Installation de node:** [Installer node js](https://nodejs.org/en/download/) [Version LTS recommandée].

### Configuration locale

* Installer les dépendances

```
npm install
```

* Exécuter localement

```
npm run dev
```

Après cela, il ouvrira un aperçu du modèle dans votre navigateur par défaut, surveillera les modifications apportées aux fichiers sources et rechargera le navigateur en direct lorsque les modifications seront enregistrées.

## Construction de la production

Après avoir terminé toutes les personnalisations, vous pouvez créer une version de production en lançant cette commande.

```
npm run build
```

## Signaler des problèmes

Si vous souhaitez poser une question, envoyez-la nous via notre [page de contact] (https://toobib.org/contact).

## Licence
Notre code est publié sous la licence [MIT](https://en.wikipedia.org/wiki/MIT_License).

**Licence des images:** Les images sont uniquement destinées à la démonstration. Elles ont leur propre licence, nous n'avons pas la permission de partager ces images.
