import config from "@config/config.json";
import theme from "@config/theme.json";
import Head from "next/head";
import { useEffect, useState } from "react";
import TagManager from "react-gtm-module";
import "styles/style.scss";
import localFont from 'next/font/local';
import { SessionProvider } from "next-auth/react"

const luciole = localFont({
  src: [
{	path: '../asset/Luciole/Luciole-Regular.ttf',
  	weight: '300',
}],
fallback: ['Helvetica'],
})

const App = ({ Component, session, pageProps }) => {
  // default theme setup
  // import google font css
  // const pf = theme.fonts.font_family.primary;

  const pf = theme.fonts.font_family.primary;
  const sf = theme.fonts.font_family.secondary;
  const [fontcss, setFontcss] = useState();

return (
    <>
	<style jsx global> {`
		:root {
			--font-base: ${luciole.style.fontFamily};
}
`}</style>

     <Head>
	<base target="_blank"></base>
        {/* responsive meta */}
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=5"
        />
      </Head>
			<SessionProvider session={session}>
      	<Component {...pageProps} />
			</SessionProvider>
    </>
  );
};

export default App;
