---
title: Politique de confidentialité
layout: politique-de-confidentialite
draft: false
articles:
  - title: En Travaux
    description: "La Politique de confidentialité est en cours de rédaction."
  - title: Collecte des données
    description: "Le site assure à l’utilisateur une collecte et un traitement d’informations personnelles dans le respect de la vie privée conformément à la loi n°78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés, au RGPD et à notre politique de confidentialité. En vertu de la loi Informatique et Libertés, en date du 6 janvier 1978 et dans le respect du RGPD, l’utilisateur dispose d’un droit d’accès, de rectification, de suppression et d’opposition de ses données personnelles. L’utilisateur exerce ce droit par mail à dpo-interhop@riseup.net"
  - title: La finalité
    description: "La finalité de l’usage des données à caractère personnel collectées reste uniquement la finalité prévue au départ."
  - title: La conservation
    description: "Les données sont conservées pendant toute la durée nécessaire aux finalités pour lesquelles elles ont été communiquées à Toobib, dans le respect de la réglementation applicable et sont, en tout état de cause, détruites à l’issue de celle-ci."
  - title: Propriété intellectuelle
    description: "Les marques, logos, signes ainsi que tous les contenus du site (textes, images, son…) font l’objet d’une protection par le Code de la propriété intellectuelle et plus particulièrement par le droit d’auteur. L’utilisateur doit solliciter l’autorisation préalable du site pour toute reproduction, publication, copie des différents contenus. Il s’engage à une utilisation des contenus du site dans un cadre strictement privé. Toute représentation totale ou partielle du site goupile.fr par quelque procédé que ce soit, sans l’autorisation expresse de l’exploitant du site Internet constituerait une contrefaçon sanctionnée par l’article L 335-2 et suivants du Code de la propriété intellectuelle. Il est rappelé conformément à l’article L122-5 du Code de propriété intellectuelle que l’utilisateur qui reproduit, copie ou publie le contenu protégé doit citer l’auteur et sa source."
  - title: Responsabilité
    description: "Les sources des informations diffusées sur le site toobib.org sont réputées fiables mais les sites ne garantissent pas qu’il soit exempt de défauts, d’erreurs ou d’omissions. Les informations communiquées sont présentées à titre indicatif et général sans valeur contractuelle. Malgré des mises à jour régulières, les sites interhop.org et goupile.fr ne peuvent être tenus responsables de la modification des dispositions administratives et juridiques survenant après la publication. De même, les sites ne peuvent être tenus responsables de l’utilisation et de l’interprétation de l’information contenue dans ce site. L’utilisateur s’assure de garder précieusement son identifiant et son mot de passe secret. Toute divulgation de l’identifiant et du mot de passe, quelle que soit la forme, est interdite. L’utilisateur assume les risques liés à l’utilisation de son identifiant et mot de passe. Le site décline toute responsabilité. Le site toobib.org ne peut être tenu pour responsables d’éventuels virus qui pourraient infecter l’ordinateur ou tout matériel informatique de l’Internaute, suite à une utilisation, à l’accès, ou au téléchargement provenant de ce site. La responsabilité du site ne peut être engagée en cas de force majeure ou du fait imprévisible et insurmontable d’un tiers.  InterHop s'engage à prévenir la fuite de données par tous les moyens de sécurité à sa disposition."
  - title: Liens hypertextes
    description: "Des liens hypertextes peuvent être présents sur le site. L’utilisateur est informé qu’en cliquant sur ces liens, il sortira du site toobib.org. Ces derniers n’ont pas de contrôle sur les pages web sur lesquelles aboutissent ces liens et ne sauraient, en aucun cas, être responsables de leurs contenus."
  - title: Cookies
    description: "Les services listés dans ces CGU n'utilisent pas de cookies."
  - title: Droit applicable et juridiction compétente
    description: "La législation française s’applique au présent contrat. En cas d’absence de résolution amiable d’un litige né entre les parties, les tribunaux français seront seuls compétents pour en connaître."
  - title: Projet hors cadre
    description: "Si votre projet sort du cadre défini par les CGU (projets recherche, services HDS par exemple), vous devez contacter la Déléguée à la Protection des données à dpo-interhop@riseup.net. Ce type de projet nécessitera l'élaboration d'un contrat."

---
