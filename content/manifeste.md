---
title: Manifeste
layout: manifeste
draft: false
intro: "Nous apportons ici les valeurs qui nous semblent essentielles de préciser avant de nous rejoindre dans cette belle aventure humaine."
articles:
  - title: Confiance
    descriptions:
      - text: "comme dans la relation de soin, la confiance est une valeur centrale de l'association. Grâce à elle, nous souhaitons ancrer le projet associatif dans une dynamique d'entraide."
  - title: Bienveillance
    descriptions: 
      - text: "les membres de l'association doivent observer une écoute qualitative des besoins individuels et du collectif, pour que chacun trouve sa place au sein du projet."
  - title: Interprofessionnel
    descriptions: 
      - text: "nous sommes convaincus que la richesse du groupe viendra de sa diversité et de la représentativité des différents corps de métiers en santé. Nous souhaitons lutter contre les rapports de force pouvant parfois s'exprimer entre différents corps de métier."
  - title: Autonomie
    descriptions: 
      - text: "ce projet cherche à développer des alternatives numériques rendant les patient.e.s et les soignant.e.s plus autonomes. L'autonomie est différente de la souveraineté numérique. En effet, elle peut s’entendre à plusieurs niveaux : des individus, des institutions (sécurité sociale, régions, départements, villes, hôpitaux …), des Nations, de l’Europe, des États-Plateformes ou même de l’humanité."
  - title: Opensource
    descriptions:
      - text: "ce terme renvoie au fait que le code source des logiciels utilisé par l'association est librement auditable, éxecutable et modifiable. Nous associons un principe éthique fort pour que les développements numériques soient pensés dans le bénéfice des soigné.e.s."
  - title: Sécurité
    descriptions:
      - text: "Les logiciels opensources sont généralement considérés comme étant plus sûrs, car ils sont souvent examinés par une communauté de développeurs spécialisée qui peut identifier et corriger les vulnérabilités signalées par les utilisateurs plus rapidement qu’avec les logiciels propriétaires. Les membres de l'association s'engagent à implémenter les standard de sécurité informatique. Pour maintenir la confiance des soigné.e.s et donc préserver la relation de soin, nous nous engageons à protéger les données de santé, sensibles par nature."
  - title: Décentralisation
    descriptions:
      - text: "nous souhaitons promovoir un internet déconcentré. Nous travaillons donc en réseau où le collectif à une place centrale"

---
