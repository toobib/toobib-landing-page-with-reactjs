---
title: Mentions légales
layout: mentions
draft: false
articles:
  - title: Code entreprises
    list:
      - text: "SIREN : XXX XXX XXX"
      - text: "SIRET : XXX XXX XXX XXXXX"
      - text: "NAF (Nomenclature d’Activités Française) : XXXXXZ"
      - text: "RNA (Répertoire National des Association) : WXXXXXXX"
      - text: "Numéro TVA IC : FRXXXXXXXX"
  - title: Nous retrouver
    list:
      - text: "Siège social : Chez Céline Decultot, 7 bis allée Neuve, 54520 LAXOU"
      - text: "Site Web : toobib.org/"
      - text: "Courriel : contact@toobib.org"
  - title: L'équipe
    list: 
      - text: "Responsable de traitement et directeur de la publication : Céline Decultot, Président de l’Association Toobib"
      - text: "Déléguée à la protection des données (DPD): Chantal CHARLOT @: dpo-toobib@riseup.net"
  - title: Cookies
    descriptions: 
      - text: "L’association Toobib édite Toobib.org. Toobib n’utilisent pas de cookies. En ce qui concerne les dons (page : toobib.org/dons notamment) Toobib utilise les widgets du site HelloAsso.com qui contient des traceurs “Google Tag Manager”.<br />Lorsqu’un service opensource tierce sera délivré par Toobib les cookies non essentiels seront systématiquement désactivés."
  
  - title: Hébergeur des sites
    descriptions:
      - text: "Toobib est hébergé en France chez Framagit."
  - title: Informations personnelles
    descriptions: 
      - text: "Conformément aux dispositions de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés, et au Règlement Général sur la Protection des données, aucune information personnelle n’est collectée à votre insu ou cédée à des tiers. Pour exercer vos droits ou éclaircir une information intégrée à la politique de confidentialité de Toobib, vous pouvez contacter la Déléguée à la Protection des Données (DPD). Pour cela, il vous suffit d’envoyer un courriel à dpo-Toobib@riseup.net ou un courrier par voie postale à Association Too Chez Céline Decultot, 7 bis allée Neuve, 54520 LAX, en justifiant de votre identit�"
  - title: "Utilisation d’images et d’icônes"
    list:
      - text: "Icônes : flaticon.com, iconfinder.com"
      - text: "Logo Toobib : Merci à Quentin PARROT"
      - text: "Design Toobib : Merci à Cyril Verneuil"
  - title: Thèmes React utilisés
    descriptions: 
      - text: "Pour Toobib.org, merci à Gethugothemes et Toobib"
post_scriptum: Vous retrouvez la politique de confidentialité de Toobib [ici](/politique-de-confidentialite).<br />Document révisé le 06 Septembre 2023 


---
