---

banner:
  title: La plateforme web pour des services numériques éthiques en santé 
  content: Toobib met à disposition une boîte à outils libre, open-source et éthique pour les patients et les professionnels de santé. L'association sensibilise et forme les professsionels de santé aux enjeux du numérique. 
  image: /images/banner-art.svg
  button_solid:
    label: "Nous contacter"
    href: "/contact"
    rel: ""

# feature
feature: 
  title: Nos valeurs
  features:
    - icon: "/images/user-clock.svg"
      title: "Gouvernance associative"
      desc: "Toutes nos décisions sont prises par des professionnels de santé"
    - icon: "/images/love.svg"
      title: "Confiance"
      desc: "Aucune utilisation secondaire des données sans un consentement formel"
    - icon: "/images/code.svg"
      title: "Les communs"
      desc: "Nous agissons dans une logique de commun. Nos solutions sont libres et open-source"
      href: "https://framagit.org/toobib"
    - icon: "/images/cloud.svg"
      title: "Sécurité"
      desc: "Nous utilisons des serveurs labelisés Hébergeur de Données de Santé."
    - icon: "/images/speedometer.svg"
      title: "Conformité"
      desc: "Nos solutions sont conformes aux exigences légales"
    - icon: "/images/decentralized.svg"
      title: "Décentralisation"
      desc: "Nous mettons en place des solutions techniquement résilentes "

# services
services:
  - title: "Nous respectons le RGPD et sécurisons des données patients-praticiens"
    content: "Le RGPD (Règlement Général sur la Protection des Données) joue un rôle crucial dans la conception de notre solution. En garantissant la confidentialité et la sécurité des données de santé, le RGPD assure la confiance des utilisateurs. Notre engagement envers le respect de ces normes renforce l'intégrité de notre plateforme et préserve la vie privée de chacun."
    images: 
      - "/images/service-slide-1.png"
    call_to_action:
      label: Participe à l'aventure toobib
      href: /contact
  
  - title: "Nous promouvons les solutions libres"
    content: "Une solution open-source présente l'avantage de la transparence et de la collaboration collective. Elle permet à la communauté de contribuer, améliorer et personnaliser le produit. Cela favorise l'innovation rapide, réduit les coûts de développement et offre une flexibilité précieuse pour répondre aux besoins variés des utilisateurs."
    images:
      - "/images/service-slide-3.png"
    call_to_action:
      label: Participe à l'aventure toobib
      href: /contact

  - title: "Nous souhaitons un internet décentralisé"
    content: "Un internet décentralisé garantit la résilience et la liberté. En éliminant les points de contrôle uniques, il réduit les risques de censure et de perte de données. Les utilisateurs ont un contrôle accru sur leurs informations, stimulant ainsi l'innovation et la diversité. Un internet décentralisé favorise un écosystème en ligne plus ouvert et démocratique."
    images:
      - "/images/service-slide-2.png"
    call_to_action:
      label: Participe à l'aventure toobib
      href: /contact

# call_to_action
call_to_action:
  title: Nous contacter
  desc: Vous souhaitez nous faire parvenir des remarques ou participer au project ? Contactez-nous !
  image: '/images/cta.svg'
  button_solid:
    label: "Contactez-nous"
    href: "/contact"
    rel: ""
---
