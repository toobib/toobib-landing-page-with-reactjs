---
title: Conditions Générales d'Utilisation
layout: cgu
draft: false
intro:
  - title: CGU 
    descriptions: 
      - text: "Les présentes conditions générales d’utilisation (dites « CGU ») seront disponibles dans un futur proche."
      - text: Ces CGU ne concernent pas les services HDS pour lesquels un contrat est établi. Toute inscription ou utilisation de nos services non HDS implique l’acceptation sans aucune réserve ni restriction des présentes CGU par l’utilisateur.En cas de non-acceptation des CGU stipulées dans le présent "contrat", l’utilisateur se doit de renoncer à l’accès des services proposés par « Toobib ». [Toobib](https://toobib.org/), pour améliorer ses services, se réserve le droit de modifier unilatéralement et à tout moment le contenu des présentes CGU. Tous nos services reposent sur des logiciels opensources voire libres (FLOSS).
    services:
      - "[pad.interhop.org](https://pad.interhop.org/)"
      - "[cpad.interhop.org](https://cpad.interhop.org/)"
      - "[password.interhop.org](https://password.interhop.org/)"
      - "[matrix](matrix.interhop.org)"
      - "[element.interhop.org](https://element.interhop.org/)"

articles:
  - title: Les mentions légales
    description: Vous trouvez nos mentions légales [ici](/mentions).
  - title: Accès aux services
    features:
      - title: Pad 
        description: "Pad est un espace d’écriture collaborative, comme une page blanche, en ligne qui se synchronise au fur et à mesure."
        acces: "Pour y accéder : [pad.interhop.org](https://pad.interhop.org/)"

      - title: CPad 
        description: "CryptPad est une alternative aux services cloud propriétaires (Drive). Ce logiciel est respectueux de la vie privée. Tout le contenu stocké dans CryptPad est chiffré de bout-en-bout, i.e avant d’être envoyé, ce qui signifie que personne (même pas nous !) ne peut accéder à vos données à moins que vous ne lui donniez les clés."
        acces: "Pour y accéder :  [cpad.interhop.org](https://cpad.interhop.org/)"

      - title: Gestionnaire de mots de passe
        description: "Un gestionnaire de mots de passe, c’est un coffre-fort virtuel pour stocker l'ensemble de ses mots de passe (banque, impôts, plateforme de formation, assurances, sites d’achat…)."
        acces: "Pour y accéder : [password.interhop.org](https://password.interhop.org/)"

      - title: Matrix et Element
        description: "Matrix ou element est une applications de chat sécurisée. Elle vous permet de garder le contrôle de vos conversations, à l’abri de l’exploitation de données et des publicités. Les échanges réalisés via Matrix et Element sont chiffrés de bout en bout."
        acces: "Pour y accéder : matrix.interhop.org et [element.interhop.org](https://element.interhop.org/)"
      
      - title: information
        description: "Pour information, tous les frais supportés par l’utilisateur pour accéder au service (matériel informatique, connexion Internet, etc.) sont à sa charge. InterHop s'efforce de maintenir le service en fonctionnement nominal. Tout événement dû à un cas de force majeure ayant pour conséquence un dysfonctionnement, quel qu’il soit, et sous réserve de toute interruption ou modification en cas de maintenance, n’engage pas la responsabilité d'InterHop. Vous avez besoin d’un de nos services, vous êtes utilisateur, vous avez la possibilité de nous contacter par messagerie électronique à l’adresse email suivante : [interhop@riseup.net](mailto:interhop@riseup.net)"
  
  - title: Collecte des données
    description: "Le site assure à l’utilisateur une collecte et un traitement d’informations personnelles dans le respect de la vie privée conformément à la loi n°78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés, au RGPD et à [notre politique de confidentialité](https://interhop.org/politique-confidentialite/). En vertu de la loi Informatique et Libertés, en date du 6 janvier 1978 et dans le respect du RGPD, l’utilisateur dispose d’un droit d’accès, de rectification, de suppression et d’opposition de ses données personnelles. L’utilisateur exerce ce droit par mail à [dpo-interhop@riseup.net](mailto:dpo-interhop@riseup.net)"
    rights:
      - title: La finalité
        description: "La finalité de l’usage des données à caractère personnel collectées reste uniquement la finalité prévue au départ."
      - title: La conservation
        description: Les données sont conservées pendant toute la durée nécessaire aux finalités pour lesquelles elles ont été communiquées à InterHop, dans le respect de la réglementation applicable et sont, en tout état de cause, détruites à l’issue de celle-ci.

  - title: Propriété intellectuelle
    description: Les marques, logos, signes ainsi que tous les contenus du site (textes, images, son…) font l’objet d’une protection par le Code de la propriété intellectuelle et plus particulièrement par le droit d’auteur. L’utilisateur doit solliciter l’autorisation préalable du site pour toute reproduction, publication, copie des différents contenus. Il s’engage à une utilisation des contenus du site dans un cadre strictement privé. Toute représentation totale ou partielle du site [toobib.org](https://toobib.org) par quelque procédé que ce soit, sans l’autorisation expresse de l’exploitant du site Internet constituerait une contrefaçon sanctionnée par l’article L 335-2 et suivants du Code de la propriété intellectuelle. Il est rappelé conformément à l’article L122-5 du Code de propriété intellectuelle que l’utilisateur qui reproduit, copie ou publie le contenu protégé doit citer l’auteur et sa source.
  
  - title: Responsabilité
    description: "Les sources des informations diffusées sur le site [toobib.org](https://toobib.org) sont réputées fiables mais les sites ne garantissent pas qu’il soit exempt de défauts, d’erreurs ou d’omissions. Les informations communiquées sont présentées à titre indicatif et général sans valeur contractuelle. Malgré des mises à jour régulières, le site [toobib.org](https://toobib.org) ne peut être tenus responsables de la modification des dispositions administratives et juridiques survenant après la publication. De même, le site ne peut être tenus responsables de l’utilisation et de l’interprétation de l’information contenue dans ce site. L’utilisateur s’assure de garder précieusement son identifiant et son mot de passe secret. Toute divulgation de l’identifiant et du mot de passe, quelle que soit la forme, est interdite. L’utilisateur assume les risques liés à l’utilisation de son identifiant et mot de passe. Le site décline toute responsabilité. Le site [toobib.org](https://toobib.org) ne peut être tenu pour responsable d’éventuels virus qui pourraient infecter l’ordinateur ou tout matériel informatique de l’Internaute, suite à une utilisation, à l’accès, ou au téléchargement provenant de ce site. La responsabilité du site ne peut être engagée en cas de force majeure ou du fait imprévisible et insurmontable d’un tiers."

  - title: Liens hypertextes
    description: Des liens hypertextes peuvent être présents sur le site. L’utilisateur est informé qu’en cliquant sur ces liens, il sortira du site [toobib.org](https://toobib.org). Ce dernier n’a pas de contrôle sur les pages web sur lesquelles aboutissent ces liens et ne sauraient, en aucun cas, être responsable de leurs contenus.

  - title: Cookies
    description: Les services listés dans ces CGU n'utilisent pas de cookies.

  - title: Droit applicable et juridiction compétente
    description: La législation française s’applique au présent contrat. En cas d’absence de résolution amiable d’un litige né entre les parties, les tribunaux français seront seuls compétents pour en connaître. Pour toute question relative à l'utilisaton de Goupile vous pouvez joindre l’éditeur aux coordonnées inscrites à l’article 1. Par contre InterHOP n'est pas éditeurs des autres services listés dans ces CGU.
  - title: Projet hors cadre
    description: Si votre projet sort du cadre défini par les CGU (projets recherche, services HDS par exemple), vous devez contacter la Déléguée à la Protection des données à <a href="mailto:dpo-interhop@riseup.net">dpo-interhop@riseup.net</a>. Ce type de projet nécessitera l'élaboration d'un contrat.

---
