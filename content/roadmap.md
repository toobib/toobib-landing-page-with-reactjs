---
title: Feuille de route
layout: roadmap
draft: false
mission: 
  - title: Mission
    answer: Toobib est d’abord un annuaire libre et open-source des professionnels de santé. Il permettra la mise en relation entre les praticiens et les patients dans un environnement libre et open source. Ce projet n’utilise que des technologies opensources!

objs:
  - title: Objectifs
    answer: 
      - Respecter la vie privée et des données personnelles des utilisateurs.trices
      - Consulter l’annuaire des professionnel.le.s de santé référencés sur Toobib
      - Vérifier et corriger les données stockées chez [annuaire.sante.fr](https://annuaire.sante.fr)
      - Créer et réserver des plages de rendez-vous

par:	
  title: Planning de réalisation
  goals:
    - year: 2022
      items:
        - Faire l'état de l'art des logiciels de prises de rendez-vous opensources existants.
        - Commencer à utiliser et adapter un logiciel opensource de prise de rendez-vous
    - year: 2023
      items:
        - Poursuite du développement du logiciel opensouce de prise de rendez-vous
        - Déploiement d'un [Cryptpad](https://cryptpad.hds.interhop.org) HDS, et tutoriel pour aide à son utilisation
        - Déploiement d'un [BitWarden](https://password.interhop.org), et tutoriel pour aide à son utilisation
        - Création d'un [dossier patient associatif et opensource](https://interhop.org/2023/06/06/fabrique-des-santes) HDS, et tutoriel pour aide à son utilisation
        - Création d’un formulaire de recherche permettant de questionner la base de données des professionnel.le.s de santé et d’afficher les résultats correspondants sur [Toobib.org](https://toobib.org/)
        - Intégration d’un système d'inscription et identification pour les professionnels
        - Création d’une page professionnelle permettant d’indiquer en détail les informations du praticien
        - Découverte de l'API Pro Santé Connect
        - Création de l'association Toobib
    - year: 2024
      items:
        - Déploiement d'une boite d'e-mail interfacée avec Mailis
        - Déploiement d'un gestionnaire d'authentification (Keycloak) HDS
        - Extension des services HDS  (visioconférence, messagerie instantanée ...)
        - Authentification via la e-CPS
        - Début des travaux de certification du Logiciel d'Aide a la Prescription (LAP)
        - Mise en place d'une messagerie instantannée et fédéree (element/matrix).
        - Mise en production d'outils de visio-conférence dediés à la santé.
---
