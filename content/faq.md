---
title: Foire aux Questions
layout: faq
draft: false
faqs: 
  - title: "Quels sont les professionnels de santé concernés par Toobib ?"
    answer: "Notre objectif est de fournir nos services à tous les professionnels de santé. Dans un premier temps, nos services se focalisent sur les besoins des sage-femmes et des médecins."
  - title: "Où sont stockées les données ?"
    answer: "Nos données sensibles sont stockées sur des serveurs HDS sous juridiction française et européenne. Notre infrastructure est open source."
  - title: "Quels sont les services numériques concernés ?"
    answer: "Toobib.org a pour objectif de fournir tous les services numériques répondant aux besoins des praticiens de santé. Nous souhaitons en premier lieu fournir une boîte de messagerie électronique et un dossier patient avec ToobibPro."
  - title: "Comment seront financés les développements futurs ?"
    answer: "Les développements futurs seront financés uniquement par les cotisations des membres."
---
